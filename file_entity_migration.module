<?php

/**
 * @file
 * Primary module hooks for File Entity to Media migration module.
 */

use Drupal\field\Plugin\migrate\source\d7\Field;
use Drupal\field\Plugin\migrate\source\d7\FieldInstance;
use Drupal\field\Plugin\migrate\source\d7\ViewMode;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\FieldMigration;

/**
 * Implements hook_migration_plugins_alter().
 */
function file_entity_migration_migration_plugins_alter(array &$migrations) {
  foreach ($migrations as &$migration) {
    /** @var \Drupal\migrate\Plugin\MigrationPluginManager $migration_plugin_manager */
    $migration_plugin_manager = \Drupal::service('plugin.manager.migration');
    $migration_stub = $migration_plugin_manager->createStubMigration($migration);
    /** @var \Drupal\migrate\Plugin\MigrateSourcePluginManager $source_plugin_manager */
    $source_plugin_manager = \Drupal::service('plugin.manager.migrate.source');
    $source = NULL;
    $configuration = $migration['source'];
    $source = $source_plugin_manager->createInstance($migration['source']['plugin'], $configuration, $migration_stub);
    if ($source) {
      if (is_a($migration['class'], FieldMigration::class, TRUE)) {

        // Field storage, instance, widget and formatter migrations.
        if (is_a($source, Field::class) || is_a($source, FieldInstance::class)) {
          _file_entity_migration_map_file_to_media_bundle($migration, 'entity_type');
        }
      }

      // View Modes.
      if (is_a($source, ViewMode::class)) {
        _file_entity_migration_map_file_to_media_bundle($migration, 'targetEntityType');
      }
    }
  }

}

/**
 * Adds static mapping from file to media to a migrate process.
 *
 * @param array $migration
 *   The migration to alter.
 * @param string $mapping_source
 *   The process mapping identifier.
 */
function _file_entity_migration_map_file_to_media_bundle(array &$migration, $mapping_source) {
  $entity_type_process = $migration['process'][$mapping_source];

  if (isset($entity_type_process['file_entity'])) {
    return;
  }
  $entity_type_process = _file_entity_migration_make_associative($entity_type_process);

  $entity_type_process['file_entity'] = [
    'plugin' => 'static_map',
    'map' => [
      'file' => 'media',
    ],
    'bypass' => TRUE,
  ];
  $migration['process'][$mapping_source] = $entity_type_process;
}

/**
 * Ensures that a plugin process mapping is an associative array.
 *
 * @param array|string $plugin_process
 *   The plugin process mapping.
 *
 * @return array
 *   The plugin process mapping as an associative array.
 */
function _file_entity_migration_make_associative($plugin_process) {
  if (!is_array($plugin_process)) {
    $plugin_process = [
      [
        'plugin' => 'get',
        'source' => $plugin_process,
      ],
    ];
  }
  elseif (array_key_exists('plugin', $plugin_process)) {
    $plugin_process = [$plugin_process];
  }

  return $plugin_process;
}

/**
 * Implements hook_migrate_prepare_row().
 */
function file_entity_migration_migrate_prepare_row(Row $row, MigrateSourceInterface $source, MigrationInterface $migration) {
  // Change the type from file to file_entity so it can be processed by
  // a migrate field plugin.
  // @see \Drupal\file_entity_migration\Plugin\migrate\field\FileEntity
  if (in_array($migration->getSourcePlugin()->getPluginId(), [
    'd7_field',
    'd7_field_instance',
    'd7_field_instance_per_view_mode',
    'd7_view_mode',
  ])) {
    if ($row->getSourceProperty('type') == 'file') {
      $row->setSourceProperty('type', 'file_entity');
    }
  }
}
